from service.api.apiMetodos import send_lecturas_to_api
import requests
from models.LecturasModels import insert_data_Lecturas

def verificar_conexion_a_internet(url='http://www.google.com'):
    try:
        response = requests.get(url, timeout=5)
        if response.status_code == 200:
            print("Hay conexión a Internet.")
        else:
            print("No hay conexión a Internet.")
    except requests.ConnectionError:
        print("No hay conexión a Internet.")

def process_data(serial_config, dato, fecha_hora):
    if verificar_conexion_a_internet():
        data = {"serial_config": serial_config, "dato": dato, "fecha_hora": fecha_hora}
        response = send_lecturas_to_api([data])
        if not response:
            
            insert_data_Lecturas(serial_config, dato, fecha_hora)
    else:
        
        insert_data_Lecturas(serial_config, dato, fecha_hora)

