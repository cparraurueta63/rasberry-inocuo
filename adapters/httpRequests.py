import requests

def send_post(url, data):
    try:
        response = requests.post(url, json=data)
        if response.status_code >= 200 and response.status_code < 300 :
            print("POST request successful.")
            return response.json()
        else:
            print(f"POST request failed with status code {response.status_code}.")
            return None
    except Exception as e:
        print(f"Error making POST request: {str(e)}")
        return None

def send_get(url, params=None):
    try:
        response = requests.get(url, params=params)
        if response.status_code == 200:
            print("GET request successful.")
            return response.json()
        else:
            print(f"GET request failed with status code {response.status_code}.")
            return None
    except Exception as e:
        print(f"Error making GET request: {str(e)}")
        return None


