import threading
from service.lecturas.LecturaTermocupla import multiple_lectura_temperatura_termocupla
from service.lecturas.LecturaDHT11 import leer_sensor_dht11
from models.SensorModels import obtener_sensores_desde_bd
from service.config.insercionConfig import management_config
from models.LecturasModels import sync_data_with_api
import time



def ejecutar_lecturas():
 while True:   
    sync_data_with_api()
    management_config()

    time.sleep(5)
    threads_ds18b20 = []
    sensores_ds18b20 = obtener_sensores_desde_bd()
   
    for sensor in sensores_ds18b20:
         thread = threading.Thread(target=multiple_lectura_temperatura_termocupla, args=(sensor,))
         threads_ds18b20.append(thread)
         thread.start()
   
    thread_dht11 = threading.Thread(target=leer_sensor_dht11)
    thread_dht11.start()

    
    for thread in threads_ds18b20:
         thread.join()
    thread_dht11.join()
    print("|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||") 
    print("|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||") 
    print("|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||") 
    print("|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||") 
    print("||Ciclo completado. Esperando 5 segundos antes de comenzar de nuevo||") 
    print("||Ciclo completado. Esperando 5 segundos antes de comenzar de nuevo||") 
    print("|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||") 
    print("|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||") 
    print("|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||") 
    print("|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||") 