import requests
from datetime import datetime
from service.api.apiMetodos import send_alertas_api
from models.TwilioModels import get_config_data_twilio
from models.ApisModels import get_apis_umbral_min, get_apis_umbral_max
from models.SensorModels import  obtener_datos_sensor
from twilio.rest import Client


def alertas(promedio, fecha_hora_promedio, nombre_sensor):
    twilios_data = get_config_data_twilio()
    twilios = twilios_data.get('Config_twilio', [])
    account_sid = twilios[0]['account_sid']
    auth_token = twilios[0]['auth_token']
    from_1 = twilios[0]['from_']
    to_ = twilios[0]['to_']

    valor_min = int(get_apis_umbral_min())
    valor_max = int(get_apis_umbral_max())

    result = obtener_datos_sensor(nombre_sensor)
        
    if result:
            serial, id_fermentacion, codigo_fermentacion = result     
    
            if promedio < valor_min or promedio > valor_max:
                alerta_promedio_total = {'id_fermentacion': id_fermentacion, 'id_tipo_alerta': 7, 'fecha_envio': fecha_hora_promedio,
                                        'dato_numerico': promedio}
                print(alerta_promedio_total)
                send_alertas_api(alerta_promedio_total)
                client = Client(account_sid, auth_token)            
                fecha_formateada = datetime.strptime(fecha_hora_promedio, "%Y-%m-%dT%H:%M:%S").strftime("%Y-%m-%d %H:%M:%S")
                mensaje = (
                            f"S={serial}\n"
                            f"T={promedio} C°\n"
                            f"F={codigo_fermentacion}\n"
                            f"Fe={fecha_formateada}"
                        )
                try:
                    message = client.messages.create(
                        body=mensaje,
                        from_= from_1,
                        to=to_
                    )
                    print(message.sid)
                    print("Alerta enviada con éxito.")
                except Exception as e:
                    print(f"No se pudo enviar la alerta. Error: {str(e)}")
    

# temperatura_actual = 27
# alertas(temperatura_actual)