import sqlite3
import os

def get_config_data_twilio():
    try:
        conn = sqlite3.connect(os.path.join(os.getcwd(), 'database', 'datos.db'))
        c = conn.cursor()        

        c.execute("SELECT * FROM config_twilio LIMIT 1")
        twilio_config = c.fetchall()
        column_names = [description[0] for description in c.description]

        conn.close()
        twilio_data =  [dict(zip(column_names, row)) for row in twilio_config]

        return {'Config_twilio': twilio_data}
    
    except Exception as e:
        print('Ha ocurrido un error al obtener datos de configuración:', str(e))
        return None


def insert_data_config_twilio(account_sid, auth_token, from_, body, to_):
    try:
        conn = sqlite3.connect(os.path.join(os.getcwd(), 'database', 'datos.db'))
        c = conn.cursor()

        # Verificar si ya existe un registro en la tabla Config_twilio
        c.execute("SELECT account_sid, auth_token, from_, body, to_ FROM Config_twilio")
        data = c.fetchone()

        if data is None:
            # Si no hay registros, insertar
            c.execute("INSERT INTO Config_twilio (account_sid, auth_token, from_, body, to_, estado) VALUES (?, ?, ?, ?, ?, ?)", 
                      (account_sid, auth_token, from_, body, to_, 1))
            conn.commit()
            print('Datos insertados correctamente en la tabla Config_twilio')
        else:
            # Si hay registro, verificar y actualizar solo los campos que han cambiado
            updates = []
            params = []
            changed_fields = []

            if data[0] != account_sid:
                updates.append("account_sid = ?")
                params.append(account_sid)
                changed_fields.append(f"account_sid: {data[0]} -> {account_sid}")

            if data[1] != auth_token:
                updates.append("auth_token = ?")
                params.append(auth_token)
                changed_fields.append(f"auth_token: {data[1]} -> {auth_token}")

            if data[2] != from_:
                updates.append("from_ = ?")
                params.append(from_)
                changed_fields.append(f"from_: {data[2]} -> {from_}")

            if data[3] != body:
                updates.append("body = ?")
                params.append(body)
                changed_fields.append(f"body: {data[3]} -> {body}")

            if data[4] != to_:
                updates.append("to_ = ?")
                params.append(to_)
                changed_fields.append(f"to_: {data[4]} -> {to_}")

            if updates:
                params.append(1)  # Estado
                update_query = "UPDATE Config_twilio SET " + ", ".join(updates) + ", estado = ?"
                c.execute(update_query, tuple(params))
                conn.commit()
                print('Los valores han sido actualizados correctamente en la tabla Config_twilio')
                for field in changed_fields:
                    print(f'Campo actualizado: {field}')
            else:
                print('No se realizaron cambios ya que los valores son los mismos.')

        conn.close()
    except Exception as e:
        print(f'Ha ocurrido un error al insertar datos de Config_twilio: {e}')

