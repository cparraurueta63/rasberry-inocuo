import sqlite3
import os

def get_config_data_sensors():
    try:
        conn = sqlite3.connect(os.path.join(os.getcwd(), 'database', 'datos.db'))
        c = conn.cursor()
       
        c.execute("SELECT serial, serial_config FROM sensores")
        sensor_config = c.fetchall()       

        column_names = [description[0] for description in c.description]
        
        conn.close()

        sensor_data = [dict(zip(column_names, row)) for row in sensor_config]

        return {'sensores': sensor_data} 
    except Exception as e:
        print('Ha ocurrido un error al obtener datos de configuración:', str(e))
        return None
    
def get_config_data_sensors_dht11():
    try:
        conn = sqlite3.connect(os.path.join(os.getcwd(), 'database', 'datos.db'))
        c = conn.cursor()
       
        c.execute("SELECT serial, serial_config, pin_dispositivo FROM sensores WHERE estado = 1 AND referencia = 'DHT11'")
        sensor_config = c.fetchall()       

        column_names = [description[0] for description in c.description]
        
        conn.close()

        sensor_data = [dict(zip(column_names, row)) for row in sensor_config]

        return sensor_data 
    except Exception as e:
        print('Ha ocurrido un error al obtener datos de configuración:', str(e))
        return None
    
def obtener_sensores_desde_bd():
    sensores = []
    try:
        conn = sqlite3.connect(os.path.join(os.getcwd(), 'database', 'datos.db'))
        c = conn.cursor()
        c.execute("SELECT serial_config FROM sensores WHERE referencia = 'DS18B20' AND estado = 1")
        result = c.fetchall()
        sensores = [row[0] for row in result]
        conn.close()
       
    except Exception as e:
        print('Error al consultar la base de datos____:', str(e))
    return sensores

def obtener_datos_sensor(nombre_sensor):
    try:
        conn = sqlite3.connect(os.path.join(os.getcwd(), 'database', 'datos.db'))
        c = conn.cursor()
        c.execute("""
            SELECT serial, id_fermentacion, codigo_fermentacion 
            FROM sensores 
            WHERE serial_config = ?
        """, (nombre_sensor,))
        result = c.fetchone()
        conn.close()
        return result
    except Exception as e:
        print('Error al consultar la base de datos:', str(e))
        return None

def insert_data_config_sensores(serial, referencia, serial_config, pin_dispositivo, estado, id, codigo):
    try:
        conn = sqlite3.connect(os.path.join(os.getcwd(), 'database', 'datos.db'))
        c = conn.cursor()

        # Verificar si el serial ya existe en la tabla sensores
        c.execute("SELECT referencia, serial_config, pin_dispositivo, estado, id_fermentacion, codigo_fermentacion FROM sensores WHERE serial = ?", (serial,))
        data = c.fetchone()

        if data is None:
            # Verificar si hay algún registro con referencia DHT11
            c.execute("SELECT COUNT(*) FROM sensores WHERE referencia = 'DHT11'")
            dht11_count = c.fetchone()[0]

            if referencia == 'DHT11' and dht11_count >= 1:
                print('Ya existe un registro con referencia DHT11. No se permite insertar otro.')
            else:
                # Insertar los datos si no hay filas coincidentes y no hay otro DHT11
                c.execute("INSERT INTO sensores (serial, referencia, serial_config, pin_dispositivo, estado, id_fermentacion, codigo_fermentacion) VALUES (?, ?, ?, ?, ?, ?, ?)", 
                          (serial, referencia, serial_config, pin_dispositivo, estado, id, codigo))
                conn.commit()
                print('Datos insertados correctamente en la tabla sensores')
        else:
            # Si el serial existe, verificar y actualizar solo los campos que han cambiado
            updates = []
            params = []
            changed_fields = []

            if data[0] != referencia:
                updates.append("referencia = ?")
                params.append(referencia)
                changed_fields.append(f"referencia: {data[0]} -> {referencia}")

            if data[1] != serial_config:
                updates.append("serial_config = ?")
                params.append(serial_config)
                changed_fields.append(f"serial_config: {data[1]} -> {serial_config}")

            if data[2] != pin_dispositivo:
                updates.append("pin_dispositivo = ?")
                params.append(pin_dispositivo)
                changed_fields.append(f"pin_dispositivo: {data[2]} -> {pin_dispositivo}")

            if data[3] != estado:
                updates.append("estado = ?")
                params.append(estado)
                changed_fields.append(f"estado: {data[3]} -> {estado}")

            if data[4] != id:
                updates.append("id_fermentacion = ?")
                params.append(id)
                changed_fields.append(f"id_fermentacion: {data[4]} -> {id}")

            if data[5] != codigo:
                updates.append("codigo_fermentacion = ?")
                params.append(codigo)
                changed_fields.append(f"codigo_fermentacion: {data[5]} -> {codigo}")

            if updates:
                update_query = "UPDATE sensores SET " + ", ".join(updates) + " WHERE serial = ?"
                params.append(serial)
                c.execute(update_query, tuple(params))
                conn.commit()
                print('Los valores han sido actualizados correctamente en la tabla sensores')
                for field in changed_fields:
                    print(f'Campo actualizado: {field}')
            else:
                print('No se realizaron cambios ya que los valores son los mismos.')

        conn.close()
    except Exception as e:
        print(f'Ha ocurrido un error al insertar datos en sensores: {e}')

