import sqlite3
import os

def insert_data_dispositivo(id_usuario, codigo, serial, referencia):
    try:
        conn = sqlite3.connect(os.path.join(os.getcwd(), 'database', 'datos.db'))
        c = conn.cursor()

        # Insertar datos en la tabla temperatura
        c.execute("INSERT INTO dispositivo (id_usuario, codigo, serial, referencia, estado) VALUES (?, ?, ?, ?, ?)", (id_usuario, codigo, serial, referencia, 1))
        conn.commit()
        print('Datos insertados correctamente en la tabla dispositivo')

        conn.close()
    except Exception as e:
        print('Ha ocurrido un error al insertar datos de dispositivo:', str(e))        

def get_dispositivo():
    try:
        conn = sqlite3.connect(os.path.join(os.getcwd(), 'database', 'datos.db'))
        c = conn.cursor()

        c.execute("SELECT serial FROM dispositivo ")
        api_config = c.fetchall()
        column_names = [description[0] for description in c.description]
        conn.close()
        dispositivo = [dict(zip(column_names, row)) for row in api_config]

        return dispositivo[0]['serial']
    except Exception as e:
        print('Ha ocurrido un error al obtener datos de configuración:', str(e))
        return None
    

