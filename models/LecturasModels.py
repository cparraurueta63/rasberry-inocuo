import sqlite3
import os
from service.api.apiMetodos import send_lecturas_to_api

def insert_data_Lecturas(serial_config, dato, fecha_hora):
    try:
        conn = sqlite3.connect(os.path.join(os.getcwd(), 'database', 'datos.db'))
        c = conn.cursor()

        # Insertar datos en la tabla temperatura
        c.execute("INSERT INTO Lecturas (serial_config, dato, fecha_hora) VALUES (?, ?, ?)", (serial_config, dato, fecha_hora))
        conn.commit()
        print('Datos insertados correctamente en la tabla lecturas')

        conn.close()
    except Exception as e:
        print('Ha ocurrido un error al insertar datos de lecturas:', str(e))        

 
def sync_data_with_api():
    try:
        conn = sqlite3.connect(os.path.join(os.getcwd(), 'database', 'datos.db'))
        c = conn.cursor()

        # Leer todos los datos de la tabla Lecturas
        c.execute("SELECT * FROM Lecturas")
        rows = c.fetchall()

        # Convertir los datos en un formato adecuado para la API
        data = [{"serial_config": row[1], "dato": row[2], "fecha_hora": row[3]} for row in rows]

        if data:
            response = send_lecturas_to_api(data)
            if response:
                # Vaciar completamente la tabla
                c.execute("DELETE FROM Lecturas")
                conn.commit()
                print('Tabla Lecturas vaciada después de enviar los datos a la API.')

                # Recrear la tabla Lecturas para reiniciar el contador de ID
                c.execute('''DROP TABLE IF EXISTS Lecturas''')
                c.execute('''CREATE TABLE Lecturas (
                                id INTEGER PRIMARY KEY,
                                serial_config TEXT,
                                dato REAL,
                                fecha_hora TEXT)''')
                conn.commit()
                print('Tabla Lecturas recreada para reiniciar el contador de ID.')

        conn.close()
    except Exception as e:
        print('Error al sincronizar datos con la API:', str(e))

