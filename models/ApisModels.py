import sqlite3
import os

def get_apis_config():
    try:
        conn = sqlite3.connect(os.path.join(os.getcwd(), 'database', 'datos.db'))
        c = conn.cursor()

        c.execute("SELECT valor FROM apis_config WHERE parametro = 'api_url_enviar'")
        api_config = c.fetchall()
        column_names = [description[0] for description in c.description]
        conn.close()
        apis_data = [dict(zip(column_names, row)) for row in api_config]

        return {'apis_config': apis_data}
    except Exception as e:
        print('Ha ocurrido un error al obtener datos de configuración:', str(e))
        return None
    
def get_apis_config_url():
    try:
        conn = sqlite3.connect(os.path.join(os.getcwd(), 'database', 'datos.db'))
        c = conn.cursor()

        c.execute("SELECT valor FROM apis_config WHERE parametro = 'api_url_configs'")
        api_config = c.fetchall()
        column_names = [description[0] for description in c.description]
        conn.close()
        apis_data = [dict(zip(column_names, row)) for row in api_config]

        return apis_data[0]['valor']
    except Exception as e:
        print('Ha ocurrido un error al obtener datos de configuración:', str(e))
        return None

def get_apis_alertas():
    try:
        conn = sqlite3.connect(os.path.join(os.getcwd(), 'database', 'datos.db'))
        c = conn.cursor()

        c.execute("SELECT valor FROM apis_config WHERE parametro = 'api_url_alerta'")
        api_config = c.fetchall()
        column_names = [description[0] for description in c.description]
        conn.close()
        apis_data = [dict(zip(column_names, row)) for row in api_config]

        return {'apis_config': apis_data}
    except Exception as e:
        print('Ha ocurrido un error al obtener datos de configuración:', str(e))
        return None
    
def get_apis_umbral_min():
    try:
        conn = sqlite3.connect(os.path.join(os.getcwd(), 'database', 'datos.db'))
        c = conn.cursor()

        c.execute("SELECT valor FROM apis_config WHERE parametro = 'valor_min'")
        api_config = c.fetchall()
        column_names = [description[0] for description in c.description]
        conn.close()
        valor_min = [dict(zip(column_names, row)) for row in api_config]

        return valor_min[0]['valor']
    except Exception as e:
        print('Ha ocurrido un error al obtener datos de configuración:', str(e))
        return None
    
def get_apis_umbral_max():
    try:
        conn = sqlite3.connect(os.path.join(os.getcwd(), 'database', 'datos.db'))
        c = conn.cursor()

        c.execute("SELECT valor FROM apis_config WHERE parametro = 'valor_max'")
        api_config = c.fetchall()
        column_names = [description[0] for description in c.description]
        conn.close()
        valor_max = [dict(zip(column_names, row)) for row in api_config]

        return valor_max[0]['valor']
    except Exception as e:
        print('Ha ocurrido un error al obtener datos de configuración:', str(e))
        return None
    
def get_duracion_total():
    try:
        conn = sqlite3.connect(os.path.join(os.getcwd(), 'database', 'datos.db'))
        c = conn.cursor()

        c.execute("SELECT valor FROM apis_config WHERE parametro = 'duracion_total'")
        api_config = c.fetchall()
        column_names = [description[0] for description in c.description]
        conn.close()
        duracion_total = [dict(zip(column_names, row)) for row in api_config]

        return duracion_total[0]['valor']
    except Exception as e:
        print('Ha ocurrido un error al obtener datos de configuración:', str(e))
        return None
    
def get_lecturas():
    try:
        conn = sqlite3.connect(os.path.join(os.getcwd(), 'database', 'datos.db'))
        c = conn.cursor()

        c.execute("SELECT valor FROM apis_config WHERE parametro = 'lecturas'")
        api_config = c.fetchall()
        column_names = [description[0] for description in c.description]
        conn.close()
        lecturas = [dict(zip(column_names, row)) for row in api_config]

        return lecturas[0]['valor']
    except Exception as e:
        print('Ha ocurrido un error al obtener datos de configuración:', str(e))
        return None

def insert_data_apis_config(parametro, valor):
    try:
        conn = sqlite3.connect(os.path.join(os.getcwd(), 'database', 'datos.db'))
        c = conn.cursor()

        # Verificar si el parámetro ya existe
        c.execute("SELECT valor FROM apis_config WHERE parametro = ?", (parametro,))
        data = c.fetchone()

        if data is None:
            # Si no hay parámetro y valor, insertar
            c.execute("INSERT INTO apis_config (parametro, valor) VALUES (?, ?)", (parametro, valor))
            conn.commit()
            print(f'Datos insertados correctamente en la tabla apis_config para el parámetro: {parametro}')
        else:
            # Si el valor del parámetro cambia, actualizar el valor del parámetro
            if data[0] != valor:
                c.execute("UPDATE apis_config SET valor = ? WHERE parametro = ?", (valor, parametro))
                conn.commit()
                print(f'El valor del parámetro "{parametro}" ha sido actualizado correctamente')
            else:
                print(f'El valor del parámetro "{parametro}" ya existe y no se insertaron cambios.')

        conn.close()
    except Exception as e:
        print(f'Ha ocurrido un error al insertar datos de apis_config para el parámetro "{parametro}": {e}')
 