import sqlite3
import os 


def initialize_database():
    
    # Conectarse a la base de datos (creará una nueva si no existe)
    print(os.path.join(os.getcwd()))
    conn = sqlite3.connect(os.path.join(os.getcwd(), 'database', 'datos.db'))
    c = conn.cursor()

    # Crear la tabla para mapeo_nombres
    # Almacenar los scripts en una variable
    create_table_scripts = """
        CREATE TABLE IF NOT EXISTS sensores
        (id INTEGER PRIMARY KEY AUTOINCREMENT,
        serial VARCHAR(100),
        referencia VARCHAR(100),
        serial_config VARCHAR(200),
        pin_dispositivo INTEGER,
        estado BOOLEAN,
        id_fermentacion INTEGER,
        codigo_fermentacion VARCHAR(100)
        );

        CREATE TABLE IF NOT EXISTS config_twilio
        (id INTEGER PRIMARY KEY AUTOINCREMENT,
        account_sid VARCHAR(200),
        auth_token TEXT,
        from_ VARCHAR(15),
        body TEXT,
        to_ VARCHAR(15),
        estado BOOLEAN);

        CREATE TABLE IF NOT EXISTS config_url_envio
        (id INTEGER PRIMARY KEY AUTOINCREMENT,
        api_url_alerta TEXT,
        api_url_enviar TEXT,
        estado BOOLEAN);

        CREATE TABLE IF NOT EXISTS dispositivo
        (id INTEGER PRIMARY KEY AUTOINCREMENT,
        id_usuario VARCHAR(15),
        codigo VARCHAR(100),
        serial VARCHAR(100),
        referencia VARCHAR(100),
        estado BOOLEAN);

        CREATE TABLE IF NOT EXISTS lecturas
        (id INTEGER PRIMARY KEY AUTOINCREMENT,
        serial_config INTEGER,
        Dato REAL,
        fecha_hora DATETIME);

        CREATE TABLE IF NOT EXISTS alertas
        (id INTEGER PRIMARY KEY AUTOINCREMENT,
        id_fermentacion INTEGER,
        id_tipo_alerta INTEGER,
        fecha_envio DATETIME);

        CREATE TABLE IF NOT EXISTS apis_config
        (id INTEGER PRIMARY KEY AUTOINCREMENT,
        parametro VARCHAR (200),
        valor VARCHAR(200));

        


    """

# Ejecutar los scripts almacenados en la variable
    c.executescript(create_table_scripts)

    # Guardar los cambios
    conn.commit()

    # Cerrar la conexión
    conn.close()
