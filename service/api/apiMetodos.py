
from adapters.httpRequests import send_post, send_get

from models.ApisModels import get_apis_config, get_apis_alertas, get_apis_config_url
from models.DispositivoModels import get_dispositivo
# def get_configs():
#     try:
#        url_configs = 'https://inocuo.cacaounicordoba.com/api/configs'
#        data = send_get(url_configs)
#        return data
#     except Exception as e:
#         print("Error al obtener los datos de la API:", str(e))

def get_configs():
    try:
        dispositivo = get_dispositivo()
        api_url = get_apis_config_url()

        if dispositivo and api_url:
            full_url = f"{api_url}{dispositivo}"
            data = send_get(full_url)
            if data:
                print("Datos obtenidos exitosamente de la API")
                return data
            else: 
                print("Fallo al obtener la configuración de la API")
                return None
        else:
            print("Fallo al obtener la configuración de la API")
            return None
    except Exception as e:
        print("Error al obtener los datos de la API:", str(e))
        return None

def send_lecturas_to_api(data):
    try:
        config_data = get_apis_config()
        if config_data and 'apis_config' in config_data and config_data['apis_config']:
            url = config_data['apis_config'][0]['valor']  
            response = send_post(url, data)
            if response:
                print("Lecturas enviadas exitosamente a la API.")
                return response
            else:
                print("Fallo al enviar las lecturas a la API.")
                return None
        else:
            print("No se encontró la configuración de la API.")
            return None
    except Exception as e:
        print(f"Error al enviar las lecturas a la API: {str(e)}")
        return None
    
def send_alertas_api(data):
    try:
        config_data = get_apis_alertas()
        if config_data and 'apis_config' in config_data and config_data['apis_config']:
            url = config_data['apis_config'][0]['valor']  
            response = send_post(url, data)
            if response:
                print("Alerta enviada exitosamente a la API.")
                return response
            else:
                print("Fallo al enviar la alerta a la API.")
                return None
        else:
            print("No se encontró la configuración de la API.")
            return None
    except Exception as e:
        print(f"Error al enviar alerta a la API: {str(e)}")
        return None

