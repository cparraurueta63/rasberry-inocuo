import time
from datetime import datetime 
#from models.LecturasModels import insert_data_Lecturas
#from service.api.apiMetodos import send_lecturas_to_api
from controllers.AlertasControllers import alertas
from adapters.internetrequests import process_data
from models.LecturasModels import sync_data_with_api
from models.ApisModels import get_duracion_total, get_lecturas

def leer_temperatura_momento(sensor):
    path = f"/sys/bus/w1/devices/{sensor}/w1_slave"
    try:
        with open(path, 'r') as file:
            lines = file.readlines()
            crc_check = lines[0].strip()[-3:]
            if crc_check == "YES":
                temp_data = lines[1].strip().split("=")[1]
                temperature = float(temp_data) / 1000.0
                return temperature
            else:
                return None
    except Exception as e:
        print(f"Error al leer el sensor {sensor}: {e}")
        return None
    

def multiple_lectura_temperatura_termocupla(sensor):
    data_ds = []
    #duracion_total = 30 #esto puede ser dinamico
    #lecturas_cada = 5 #esto puedo ser dinamico 
    duracion_total = int(get_duracion_total())
    lecturas_cada = int(get_lecturas())
    num_lecturas = duracion_total // lecturas_cada
    temperaturas = []  
    nombre_sensor = sensor  
    print(f"Sensor encontrado en la base de datos con serial_config: {nombre_sensor}")
    
    # Procesar la lectura del sensor
    print(f"Leyendo sensor {nombre_sensor} durante {duracion_total} segundos...")
    for i in range(num_lecturas):
        temperatura = leer_temperatura_momento(sensor)
        print(f"########################### temperatura: {temperatura}")
        if temperatura is not None:
            print(f"########################### dentro")
            temperaturas.append(temperatura)
        # Obtener la fecha y hora actual
        now = datetime.now()
        fecha_hora_lectura = now.strftime("%Y-%m-%dT%H:%M:%S")
        # Imprimir resultados parciales
        print(f"Sensor {nombre_sensor} - Lectura {i + 1}/{num_lecturas}:")
        print(f"Fecha y hora de lectura: {fecha_hora_lectura}")
        print(f"Temperatura actual: {temperatura} °C")
        print("-" * 40)
        if i < num_lecturas - 1:
            time.sleep(lecturas_cada)  # Esperar 5 segundos entre lecturas
    promedio = round(sum(temperaturas) / len(temperaturas), 2)
    # Obtener la fecha y hora actual para el promedio
    now = datetime.now()
    fecha_hora_promedio = now.strftime("%Y-%m-%dT%H:%M:%S")
    print("=" * 40)
    print(f"Sensor {nombre_sensor} - Promedio de {num_lecturas} lecturas:")
    print(f"Fecha y hora del promedio: {fecha_hora_promedio}")
    print(f"Promedio: {promedio} °C")
    print("=" * 40)
    
    
    #insert_data_Lecturas(nombre_sensor, promedio,  fecha_hora_promedio  )
    if promedio is not None: 
        process_data(nombre_sensor, promedio, fecha_hora_promedio)
        data_ds.append({'serial_config': nombre_sensor, 'dato': promedio, 'fecha_hora':fecha_hora_promedio })
        print(data_ds)
    
   #send_lecturas_to_api(data_ds)
    
    alertas(promedio, fecha_hora_promedio, nombre_sensor)
    
sync_data_with_api()
    