import time
import Adafruit_DHT
from datetime import datetime 
from service.api.apiMetodos import send_lecturas_to_api
from models.SensorModels import get_config_data_sensors_dht11
from adapters.internetrequests import process_data
from models.LecturasModels import sync_data_with_api
from models.ApisModels import get_duracion_total, get_lecturas
    
# sensor1 = Adafruit_DHT.DHT11
# pin1 = 12
total_temperatura_1 = 0
total_humedad_1 = 0
num_mediciones_1 = 0

def leer_sensor_dht11():
    sensores = get_config_data_sensors_dht11();

    global total_temperatura_1, total_humedad_1, num_mediciones_1
    
    # Duración de la recolección de datos
    #duracion_total = 30  # 1 minuto en segundosS
    duracion_total = int(get_duracion_total())
    lecturas_cada = int(get_lecturas())
    # Bucle para la recolección de datos
    start_time = time.time()
    
    for sensor in sensores:
        data_dht11 = []
        total_temperatura = 0
        total_humedad = 0
        num_mediciones = 0
        sensor_adapter = Adafruit_DHT.DHT11
        pin = sensor['pin_dispositivo']
        current_time = datetime.now().strftime("%Y-%m-%dT%H:%M:%S")

        while time.time() - start_time < duracion_total:
            humedad, temperatura = Adafruit_DHT.read_retry(sensor_adapter, pin)
            if humedad is not None and temperatura is not None:
                total_temperatura += temperatura
                total_humedad += humedad
                num_mediciones += 1          
                print("{0} - Sensor ", sensor['serial'], ": Temp={1:0.1f}C Hum={2:0.1f}%".format(current_time, temperatura, humedad))
            else:
                print("{0} - Sensor ", sensor['serial'], ": Fallo en la lectura, revisa el circuito".format(current_time))
            
            time.sleep(lecturas_cada)

        current_time = datetime.now().strftime("%Y-%m-%dT%H:%M:%S")
        promedio_temperatura = total_temperatura / num_mediciones
        promedio_humedad = total_humedad / num_mediciones    
       
        print("\nPromedio del Sensor ", sensor['serial'], ": Temperaturas ({0}): {1:0.1f}C".format(current_time, promedio_temperatura))
        print("Promedio del Sensor ", sensor['serial'], ": Humedad relativa({0}): {1:0.1f}%".format(current_time, promedio_humedad))
        data_dht11.append({'serial_config': sensor['serial_config'], 'dato': promedio_temperatura, 'fecha_hora': current_time})
        data_dht11.append({'serial_config': sensor['serial_config'], 'dato': promedio_humedad, 'fecha_hora': current_time})
        print(data_dht11)
        for data in data_dht11:
            process_data(data['serial_config'], data['dato'], data['fecha_hora'])
        #send_lecturas_to_api(data_dht11)
    sync_data_with_api()