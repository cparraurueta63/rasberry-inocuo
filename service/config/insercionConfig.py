from service.api.apiMetodos import get_configs
from models.TwilioModels import insert_data_config_twilio
from models.SensorModels import  insert_data_config_sensores
from models.ApisModels import   insert_data_apis_config
from service.api.apiMetodos import get_configs 
from database.database_manager import initialize_database

def management_config():
    try:
        # Inicializar la base de datos
        initialize_database()
        
        # Obtener datos de la API
        configs = get_configs()
        # print(configs["sensors"])

        
        # Insertar datos de sensores
        for sensor in configs['dispositivo']["sensores"]:
            id_sensor = sensor['fermentacion']['id'] if sensor['fermentacion'] != None else None
            codigo = sensor['fermentacion']['codigo'] if sensor['fermentacion'] != None else None
            insert_data_config_sensores(sensor["serial"], sensor["referencia"], sensor["serial_config"], sensor["pin_dispositivo"], sensor['estado'], id_sensor, codigo )
        
        
        # Insertar datos de configuración de Twilio
        twilio_config = configs["twilio_configs"]
        insert_data_config_twilio(twilio_config["account_sid"], twilio_config["auth_token"], twilio_config["from_"], twilio_config["body"], twilio_config["to"])
        
        # Insertar datos de configuración de URL de envío

        for apis_config in configs['dispositivo']["api_configs"]:
            insert_data_apis_config (apis_config["parametro"], apis_config["valor"])

        print("Proceso completado exitosamente.")
    except Exception as e:
        print("Ha ocurrido un error durante el proceso:", str(e))    